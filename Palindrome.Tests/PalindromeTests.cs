﻿using NUnit.Framework;

namespace Palindrome.Tests
{
    public class PalindromeTests
    {
        [TestCase("anna", Result = true)]
        [TestCase("aaaaaaaa", Result = true)]
        [TestCase("aaaaaaaab", Result = true)]
        [TestCase("caaaaaaaab", Result = false)]
        [TestCase("donotbobtonod", Result = true)]
        [TestCase("owefhijpfwai", Result = false)]
        [TestCase("igdedgide", Result = true)]
        [TestCase("aca", Result = true)]
        [TestCase("aabcxabca", Result = true)]
        public bool TestPalindromeOrAnagram(string input)
        {
            bool result = ConsoleApp.Palindrome.IsPalindromeOrAnagram(input);
            return result;
        }
    }
}