﻿using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("anna"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("aaaaaaaa"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("aaaaaaaab"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("caaaaaaaab"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("donotbobtonod"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("owefhijpfwai"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("igdedgide"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("aabcxabca"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("aca"));
            Console.WriteLine(Palindrome.IsPalindromeOrAnagram("xnmabmnax"));
            Console.Read();
        }
    }
}
