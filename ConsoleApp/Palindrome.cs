﻿using System;
using System.Linq;

namespace ConsoleApp
{
    public class Palindrome
    {
        public static bool IsPalindromeOrAnagram(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return false;

            bool isAnagram = false;
            bool isPalindrome = string.Equals(input, new string(input.Reverse().ToArray()));

            if (!isPalindrome && (input.Length % 2 != 0))
            {
                isAnagram = IsAnagram(input);
            }
            return (isPalindrome || isAnagram);
        }

        private static bool IsAnagram(string input)
        {
            int leftPartLength = (input.Length) / 2; //(int) Math.Floor(decimal.Divide((input.Length) , 2));

            string leftPart = Sort(input.Substring(0, leftPartLength));
            string rightPart = Sort(input.Substring(leftPartLength + 1));

            if (string.Equals(leftPart, rightPart))
                return true;

            leftPart = Sort(input.Substring(0, leftPartLength));
            rightPart = Sort(input.Substring(leftPartLength));

            if (string.Equals(leftPart, rightPart.Substring(0, leftPartLength)) ||
                string.Equals(leftPart, rightPart.Substring(1)))
            {
                return true;
            }

            return false;
        }

        private static string Sort(string input)
        {
            var chars = input.ToCharArray();
            Array.Sort(chars);
            return new string(chars);
        }
    }
}
